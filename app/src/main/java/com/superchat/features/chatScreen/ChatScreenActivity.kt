package com.superchat.features.chatScreen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.mymvc.presenter.interfaces.communications.TheServer
import com.superchat.R
import com.superchat.app.ChatApp
import com.superchat.app.PUSH_NOTIFICATION
import com.superchat.features.chatRooms.ChatRoomThreadAdapter
import com.superchat.network.models.Message
import com.superchat.network.models.SmallMessage
import com.superchat.network.models.User
import com.superchat.utils.NotificationUtils
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

/**
 * Created by david on 1/03/16.
 */
class ChatScreenActivity : AppCompatActivity() {

    private val TAG = ChatScreenActivity::class.java.simpleName

    private var chatRoomId: String? = null
    private var recyclerView: RecyclerView? = null
    private var mAdapter: ChatRoomThreadAdapter? = null
    private var messageArrayList: ArrayList<Message>? = null
    private var mRegistrationBroadcastReceiver: BroadcastReceiver? = null
    private var inputMessage: EditText? = null
    private var btnSend: Button? = null
    var selfUserId = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_screen)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        inputMessage = findViewById(R.id.message) as EditText
        btnSend = findViewById(R.id.btn_send) as Button

        val intent = intent
        chatRoomId = intent.getStringExtra("chat_room_id")
        val title = intent.getStringExtra("name")

        supportActionBar!!.setTitle(title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener({ onBackPressed() });

        if (chatRoomId == null) {
            Toast.makeText(applicationContext, "Chat room not found!", Toast.LENGTH_SHORT).show()
            finish()
        }

        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        messageArrayList = ArrayList<Message>()

        // self user id is to identify the message owner
        selfUserId = ChatApp.instance?.prefManager?.getUser()?.userId!!

        mAdapter = ChatRoomThreadAdapter(this, messageArrayList!!, selfUserId!!)

        val layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = mAdapter

        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == PUSH_NOTIFICATION) {
                    // new push message is received
                    handlePushNotification(intent)
                }
            }
        }

        btnSend!!.setOnClickListener { sendMessage() }

        fetchChatThread()
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        mRegistrationBroadcastReceiver, IntentFilter(PUSH_NOTIFICATION)
                )
        NotificationUtils.clearNotifications()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     */
    private fun handlePushNotification(intent: Intent) {
        val message = intent.getSerializableExtra("message") as Message
        val chatRoomId = intent.getStringExtra("chat_room_id")

        if (message != null && chatRoomId != null) {
            messageArrayList!!.add(message)
            mAdapter!!.notifyDataSetChanged()
            if (mAdapter!!.getItemCount() > 1) {
                recyclerView!!.layoutManager.smoothScrollToPosition(recyclerView, null, mAdapter!!.getItemCount() - 1)
            }
        }
    }

    private fun sendMessage() {
        val message = this.inputMessage!!.text.toString().trim { it <= ' ' }

        if (TextUtils.isEmpty(message)) {
            Toast.makeText(applicationContext, "Enter a message", Toast.LENGTH_SHORT).show()
            return
        }

        inputMessage!!.text.clear()

        TheServer.instance.sendMessage(chatRoomId.toString(), SmallMessage(selfUserId, message))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { message ->
                    messageArrayList?.add(message)
                    mAdapter?.notifyDataSetChanged()
                    if (mAdapter?.itemCount!! > 1) {
                        // scrolling to bottom of the recycler view
                        recyclerView?.layoutManager?.smoothScrollToPosition(recyclerView, null, mAdapter?.itemCount!! - 1);
                    }
                }
                .onErrorReturn { it: Throwable? ->
                    Toast.makeText(applicationContext, "error enviando el mensaje", Toast.LENGTH_SHORT).show()
                    Message(0, 1, User(), "")
                }
                .subscribe()
    }


    /**
     * Fetching all the messages of a single chat room
     */
    private fun fetchChatThread() {
        TheServer.instance.FetchMessages(chatRoomId.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { messages ->
                    messageArrayList?.addAll(messages)
                    mAdapter?.notifyDataSetChanged()
                    if (mAdapter?.itemCount!! > 1) {
                        // scrolling to bottom of the recycler view
                        recyclerView?.layoutManager?.smoothScrollToPosition(recyclerView, null, mAdapter?.itemCount!! - 1);
                    }
                }
                .onErrorReturn { it: Throwable? ->
                    Toast.makeText(applicationContext, "error enviando el mensaje", Toast.LENGTH_SHORT).show()
                    ArrayList()
                }
                .subscribe()
    }

}