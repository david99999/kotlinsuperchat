package com.superchat.features.chatScreen

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.superchat.network.models.Message
import com.superchat.utils.ToDateTime
import kotlinx.android.synthetic.main.chat_item_self.view.*
import kotlinx.android.synthetic.main.chat_item_other.view.message as messageOther
import kotlinx.android.synthetic.main.chat_item_other.view.timestamp as timestampOther

/**
 * Created by david on 2/03/16.
 */

class MessageView(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {
    fun setMessage(messa: Message, mine: Boolean) {
        if (mine) {
            message.text = messa.message
            timestamp.text = messa.createdAt.ToDateTime()
        } else {
            messageOther.text = messa.message
            timestampOther.text = messa.createdAt.ToDateTime()
        }
    }
}