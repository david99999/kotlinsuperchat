package com.superchat.features.chatRooms

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.superchat.R
import com.superchat.features.chatScreen.MessageView
import com.superchat.network.models.Message
import java.util.*

/**
 * Created by david on 1/03/16.
 */
class ChatRoomThreadAdapter(var context: Context,
                            var messagesArrayList: ArrayList<Message>,
                            var myUserId: Int) :
        RecyclerView.Adapter<ChatViewHolder>() {

    private val SELF: Int = 100

    override fun onBindViewHolder(holder: ChatViewHolder?, p1: Int) {
        var messa = messagesArrayList[p1]
        (holder?.itemView as MessageView)?.setMessage(messa, messa.user.userId.equals(myUserId))
    }

    override fun getItemCount(): Int {
        return messagesArrayList.size
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): ChatViewHolder? {
        return ChatViewHolder(
                LayoutInflater
                        .from(p0?.context)
                        .inflate(
                                if (p1.equals(SELF))
                                    R.layout.chat_item_self
                                else
                                    R.layout.chat_item_other,
                                p0,
                                false)
        )
    }


    override fun getItemViewType(position: Int): Int {
        var message = messagesArrayList[position];
        if (message.user.userId.equals(myUserId)) {
            return SELF;
        }
        return position;
    }
}