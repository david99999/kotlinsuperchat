package com.superchat.features.register


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.mymvc.presenter.interfaces.communications.TheServer
import com.superchat.R
import com.superchat.app.ChatApp
import com.superchat.features.chatRooms.ChatRoomsActivity
import com.superchat.network.models.User
import com.superchat.utils.ListAllChatGroupsService
import kotlinx.android.synthetic.main.activity_register.*
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class RegisterActivity : AppCompatActivity() {

    private val TAG = RegisterActivity::class.java.simpleName

    var subscription: Subscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var data = Intent(this, ListAllChatGroupsService::class.java)
        var dataBundle = Bundle()
        dataBundle.putSerializable("user", User(name = "lewandowski"))
        data.putExtras(dataBundle)
        startService(data)
        /**
         * Check for login session. It user is already logged in
         * redirect him to main activity
         */
        if (ChatApp.instance!!.prefManager.getUser() != null) {
            startActivity(Intent(this, ChatRoomsActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_register)


        input_name!!.addTextChangedListener(MyTextWatcher(input_name, { validateName() }, { validateEmail() }))
        input_email!!.addTextChangedListener(MyTextWatcher(input_email, { validateName() }, { validateEmail() }))

        btn_enter!!.setOnClickListener { login() }
    }

    /**
     * logging in user. Will make http post request with name, email
     * as parameters
     */
    private fun login() {
        if (!validateName()) {
            return
        }

        if (!validateEmail()) {
            return
        }

        val name = input_name.text.toString()
        val email = input_email!!.text.toString()
        subscription = TheServer.instance.RegisterUser(User(name, email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { user ->
                    // storing user in shared preferences
                    ChatApp.Companion.instance!!.prefManager.storeUser(user)

                    // start main activity
                    startActivity(Intent(applicationContext, ChatRoomsActivity::class.java))
                    Toast.makeText(applicationContext, "Bienvenida hermosa " + name, Toast.LENGTH_LONG).show()
                    finish()
                }
                .onErrorReturn { it: Throwable? ->
                    Toast.makeText(applicationContext, "error registrando en push", Toast.LENGTH_SHORT).show()
                    User()
                }
                .subscribe()

    }

    override fun onDestroy() {
        super.onDestroy()
        subscription?.unsubscribe()
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    // Validating name
    fun validateName(): Boolean {
        if (input_name!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            input_layout_name!!.error = getString(R.string.err_msg_name)
            requestFocus(input_name)
            return false
        } else {
            input_layout_name!!.isErrorEnabled = false
        }

        return true
    }

    // Validating email
    fun validateEmail(): Boolean {
        val email = input_email!!.text.toString().trim { it <= ' ' }

        if (email.isEmpty() || !isValidEmail(email)) {
            input_layout_email!!.error = getString(R.string.err_msg_email)
            requestFocus(input_email)
            return false
        } else {
            input_layout_email!!.isErrorEnabled = false
        }

        return true
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    class MyTextWatcher(var view: View,
                        var nameValidator: () -> Boolean,
                        var emailValidator: () -> Boolean) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.input_name -> nameValidator()
                R.id.input_email -> emailValidator()
            }
        }
    }
}