package com.superchat.features.chatRooms

import android.view.View

/**
 * Created by david on 1/03/16.
 */
interface ClickListener {
    fun onClick(view: View, position: Int)
    fun onLongClick(view: View, position: Int)
}