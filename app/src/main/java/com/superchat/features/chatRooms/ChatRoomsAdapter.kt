package com.superchat.features.chatRooms

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.superchat.R
import com.superchat.network.models.ChatRoom
import java.util.*

/**
 * Created by david on 1/03/16.
 */
class ChatRoomsAdapter(var context: Context, var chatRoomArrayList: ArrayList<ChatRoom>) :
        RecyclerView.Adapter<ChatViewHolder>() {



    override fun onBindViewHolder(holder: ChatViewHolder?, p1: Int) {
        var chatRoom = chatRoomArrayList[p1]
        (holder?.itemView as ChatView)?.setChatRoom(chatRoom)
    }

    override fun getItemCount(): Int {
        return chatRoomArrayList.size
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): ChatViewHolder? {
        return ChatViewHolder(
                LayoutInflater.from(p0?.context).inflate(R.layout.chat_rooms_list_row, p0, false)
        )
    }
}