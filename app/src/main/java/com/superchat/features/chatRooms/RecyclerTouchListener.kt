package com.superchat.features.chatRooms

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent

/**
 * Created by david on 1/03/16.
 */
class RecyclerTouchListener(var context: Context, var recyclerView: RecyclerView, var clickListener: ClickListener)
: RecyclerView.OnItemTouchListener {

    var gestureDetector: GestureDetector;

    init {
        gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent?): Boolean {
                return true
            }

            override fun onLongPress(e: MotionEvent?) {
                var child = recyclerView.findChildViewUnder(e?.x!!, e?.y!!);
                if (child != null) {
                    clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        })
    }

    override fun onTouchEvent(p0: RecyclerView?, p1: MotionEvent?) {
    }

    override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
        var child = rv?.findChildViewUnder(e?.x!!, e?.y!!);
        if (child != null && gestureDetector.onTouchEvent(e)) {
            clickListener.onClick(child, rv?.getChildAdapterPosition(child)!!);
        }
        return false;
    }

    override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {
    }

}