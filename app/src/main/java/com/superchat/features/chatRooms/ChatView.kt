package com.superchat.features.chatRooms

/**
 * Created by david on 1/03/16.
 */

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.superchat.network.models.ChatRoom
import com.superchat.utils.ToDateTime
import kotlinx.android.synthetic.main.chat_rooms_list_row.view.*

class ChatView(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    fun setChatRoom(chatRoom: ChatRoom) {
        tvname.text = chatRoom.name
        tvmessage.text = chatRoom.lastMessage
        if (chatRoom.unreadCount > 0) {
            tvcount.text = chatRoom.unreadCount.toString()
            tvcount.visibility = View.VISIBLE
        } else {
            tvcount.visibility = View.GONE
        }
        tvtimestamp.text = chatRoom.createdAt.ToDateTime()
    }
}
