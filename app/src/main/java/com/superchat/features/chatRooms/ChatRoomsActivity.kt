package com.superchat.features.chatRooms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.mymvc.presenter.interfaces.communications.TheServer
import com.squareup.otto.Subscribe
import com.superchat.R
import com.superchat.app.*
import com.superchat.features.chatScreen.ChatScreenActivity
import com.superchat.features.register.RegisterActivity
import com.superchat.network.models.ChatRoom
import com.superchat.network.models.Message
import com.superchat.utils.ChatBus
import com.superchat.utils.NotificationUtils
import com.superchat.utils.SimpleDividerItemDecoration
import com.superchat.utils.gcm.GcmIntentService
import kotlinx.android.synthetic.main.activity_chat_rooms.*
import kotlinx.android.synthetic.main.content_chat_rooms.*
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


class ChatRoomsActivity : AppCompatActivity() {

    private val TAG = ChatRoomsActivity::class.java.simpleName
    private var mRegistrationBroadcastReceiver: BroadcastReceiver? = null
    private var chatRoomArrayList: ArrayList<ChatRoom>? = null
    private var mAdapter: ChatRoomsAdapter? = null
    private var subscription: Subscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * Check for login session. If not logged in launch
         * login activity
         * */
        if (ChatApp.Companion.instance?.prefManager?.getUser() == null) {
            launchLoginActivity();
        }

        ChatBus.instance.register(this)

        setContentView(R.layout.activity_chat_rooms)
        setSupportActionBar(toolbar)

        supportActionBar!!.title = getString(R.string.app_name) + " de " + ChatApp.Companion.instance?.prefManager?.getUser()?.name
        //GCM Registration Token: fvnkBifgOwI:APA91bEE0UFI-uS6st2o9H44FQjAuSmRdvtWLzRbi3P1KUz_t70o4FW4DEiT7Xh0DZ-qyzBcyl3JMdQJBMD6BPu-G3hqPNsI-oUuicyD3K746FHEXAWnpuFC0vxzAq4Be4fFXm2CSyr_
        /**
         * Broadcast receiver calls in two scenarios
         * 1. gcm registration is completed
         * 2. when new push notification is received
         */
        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                // checking for type intent filter
                if (intent.action == REGISTRATION_COMPLETE) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    subscribeToGlobalTopic()

                } else if (intent.action == SENT_TOKEN_TO_SERVER) {
                    // gcm registration id is stored in our server's MySQL
                    Log.e(TAG, "GCM registration id is sent to our server")

                } else if (intent.action == PUSH_NOTIFICATION) {
                    // new push notification is received
                    handlePushNotification(intent)
                }
            }
        }

        chatRoomArrayList = ArrayList<ChatRoom>()
        mAdapter = ChatRoomsAdapter(this, chatRoomArrayList as ArrayList<ChatRoom>)
        val layoutManager = LinearLayoutManager(this)
        recycler_view!!.layoutManager = layoutManager
        recycler_view!!.addItemDecoration(SimpleDividerItemDecoration(
                applicationContext))
        recycler_view!!.itemAnimator = DefaultItemAnimator()
        recycler_view!!.adapter = mAdapter

        recycler_view!!.addOnItemTouchListener(RecyclerTouchListener(applicationContext, recycler_view, object : ClickListener {
            override fun onClick(view: View, position: Int) {
                // when chat is clicked, launch full chat thread activity
                val chatRoom = chatRoomArrayList!![position]
                val intent = Intent(this@ChatRoomsActivity, ChatScreenActivity::class.java)
                intent.putExtra("chat_room_id", chatRoom.chatRoomId)
                intent.putExtra("name", chatRoom.name)
                startActivity(intent)
            }

            override fun onLongClick(view: View, position: Int) {

            }
        }))

        /**
         * Always check for google play services availability before
         * proceeding further with GCM
         */
        if (checkPlayServices()) {
            registerGCM()
            fetchChatRooms()
        }
    }

    @Subscribe
    fun answerAvailable(rooms: ArrayList<ChatRoom>) {
        Log.i("tadaa", "recibidos en la actividad")
        Toast.makeText(this, " venian ${rooms.size} chats", Toast.LENGTH_SHORT).show()
    }

    /**
     * Handles new push notification
     */
    private fun handlePushNotification(intent: Intent) {
        val type = intent.getIntExtra("type", -1)

        // if the push is of chat room message
        // simply update the UI unread messages count
        if (type == PUSH_TYPE_CHATROOM) {
            val message = intent.getSerializableExtra("message") as Message
            val chatRoomId = intent.getStringExtra("chat_room_id")

            if (message != null && chatRoomId != null) {
                updateRow(chatRoomId, message)
            }
        } else if (type == PUSH_TYPE_USER) {
            // push belongs to user alone
            // just showing the message in a toast
            val message = intent.getStringExtra("message")
            Toast.makeText(applicationContext, "New push: " + message, Toast.LENGTH_LONG).show()
        }


    }

    /**
     * Updates the chat list unread count and the last message
     */
    private fun updateRow(chatRoomId: String, message: Message) {
        for (cr in chatRoomArrayList!!) {
            if (cr.chatRoomId.equals(chatRoomId)) {
                val index = chatRoomArrayList!!.indexOf(cr)
                cr.lastMessage = message.message
                cr.createdAt = message.createdAt
                cr.unreadCount = cr.unreadCount + 1
                chatRoomArrayList!!.removeAt(index)
                chatRoomArrayList!!.add(index, cr)
                break
            }
        }
        mAdapter!!.notifyDataSetChanged()
    }


    /**
     * fetching the chat rooms by making http call
     */
    private fun fetchChatRooms() {
        subscription = TheServer.instance.FetchChats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { chats ->
                    chatRoomArrayList?.clear()
                    chatRoomArrayList?.addAll(chats)
                    mAdapter?.notifyDataSetChanged()
                    subscribeToAllTopics()
                }
                .onErrorReturn { it: Throwable? ->
                    Toast.makeText(applicationContext, "error obteniendo chats", Toast.LENGTH_SHORT).show()
                    ArrayList<ChatRoom>()
                }
                .subscribe()
    }

    // subscribing to global topic
    private fun subscribeToGlobalTopic() {
        val intent = Intent(this, GcmIntentService::class.java)
        intent.putExtra(GcmIntentService.KEY, GcmIntentService.SUBSCRIBE)
        intent.putExtra(GcmIntentService.TOPIC, TOPIC_GLOBAL)
        startService(intent)
    }

    // Subscribing to all chat room topics
    // each topic name starts with `topic_` followed by the ID of the chat room
    // Ex: topic_1, topic_2
    private fun subscribeToAllTopics() {
        for (cr in chatRoomArrayList!!) {

            val intent = Intent(this, GcmIntentService::class.java)
            intent.putExtra(GcmIntentService.KEY, GcmIntentService.SUBSCRIBE)
            intent.putExtra(GcmIntentService.TOPIC, "topic_" + cr.chatRoomId)
            startService(intent)
        }
    }

    private fun launchLoginActivity() {
        val intent = Intent(this@ChatRoomsActivity, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(PUSH_NOTIFICATION))

        // clearing the notification tray
        NotificationUtils.clearNotifications()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    // starting the service to register with GCM
    private fun registerGCM() {
        val intent = Intent(this, GcmIntentService::class.java)
        intent.putExtra("key", "register")
        startService(intent)
    }

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show()
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!")
                Toast.makeText(applicationContext, "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show()
                finish()
            }
            return false
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.action_logout -> ChatApp.instance?.logout()
        }
        return super.onOptionsItemSelected(menuItem)
    }


    override fun onDestroy() {
        super.onDestroy()
        subscription?.unsubscribe()
    }

    companion object {
        private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    }
}