package com.superchat.network.constants

/**
 * Created by david on 24/02/16.
 */

const val SERVER_URL = "http://prod.gustapp.com/v1/"
const val LOGIN = "user/login"
const val USER = "user/{userId}"
const val CHAT_ROOM = "chat_rooms"
const val CHAT_THREAD = "chat_rooms/{chatRoomId}"
const val CHAT_MESSAGE = "chat_rooms/{chatRoomId}/message"

const val SEND_MESSAGE_TO_USER = "users/{targetId}/message"