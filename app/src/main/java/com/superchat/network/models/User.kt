package com.superchat.network.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by david on 24/02/16.
 */
data class User(@SerializedName("name") var name: String = "",
                @SerializedName("email") var email: String = "",
                @SerializedName("userId") var userId: Int = 0,
                @SerializedName("gcm_registration_id") var pushToken: String = "00") : Serializable
