package com.superchat.network.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by david on 2/03/16.
 */
data class SmallMessage(@SerializedName("user_id") var userId: Int,
                        @SerializedName("message") var message: String ):Serializable