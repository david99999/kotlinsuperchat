package com.superchat.network.models

import com.google.gson.annotations.SerializedName
import com.superchat.network.constants.USER_PUSH_TOKEN

/**
 * Created by david on 24/02/16.
 */
data class PushToken(@SerializedName(USER_PUSH_TOKEN) var token: String)