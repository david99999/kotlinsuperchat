package com.superchat.network.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by david on 24/02/16.
 */


data class Message(@SerializedName("messageId") var messageId: Int,
                   @SerializedName("createdAt") var createdAt: Long,
                   @SerializedName("user") var user: User,
                   @SerializedName("message") var message: String,
                   @SerializedName("chat_room_id") var chatRoomId: Int  = 1) : Serializable