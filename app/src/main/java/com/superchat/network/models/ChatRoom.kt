package com.superchat.network.models

/**
 * Created by david on 24/02/16.
 */

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChatRoom(@SerializedName("chatRoomId") var chatRoomId: String,
                    @SerializedName("name") var name: String,
                    @SerializedName("lastMessage") var lastMessage: String,
                    @SerializedName("createdAt") var createdAt: Long,
                    @SerializedName("unreadCount") var unreadCount: Int) : Serializable