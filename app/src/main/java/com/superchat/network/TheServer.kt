package com.mymvc.presenter.interfaces.communications

import android.content.Context
import com.superchat.BuildConfig
import com.superchat.network.constants.*
import com.superchat.network.models.*
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import rx.Observable
import java.util.*

/**
 * Created by david on 11/02/16.
 */
interface TheServer {

    @PUT(USER)
    fun sendRegistrationToServer(@Path("userId") userId: String, @Body userToken: PushToken): Observable<User>


    @POST(LOGIN)
    fun RegisterUser(@Body user: User): Observable<User>


    @GET(CHAT_ROOM)
    fun FetchChats(): Observable<ArrayList<ChatRoom>>


    @GET(CHAT_THREAD)
    fun FetchMessages(@Path("chatRoomId") chatRoomId: String): Observable<ArrayList<Message>>


    @POST(CHAT_MESSAGE)
    fun sendMessage(@Path("chatRoomId") chatRoomId: String, @Body message: SmallMessage): Observable<Message>


    @POST(SEND_MESSAGE_TO_USER)
    fun sendMessageForUser(@Path("targetId") targetId: String, @Body message: SmallMessage): Observable<Message>

    companion object {

        lateinit var instance: TheServer

        fun init(context: Context, url: String = SERVER_URL) {
            var builder = OkHttpClient().newBuilder()

            if (BuildConfig.DEBUG) {
                var interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY;
                builder.addInterceptor(interceptor);
            }
            builder.cache(Cache(context.cacheDir, 10 * 1024 * 1024)) //10Mb

            val restAdapter = Retrofit.Builder()
                    .baseUrl(url)
                    .client(builder.build())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            instance = restAdapter.create(TheServer::class.java)
        }
    }
}