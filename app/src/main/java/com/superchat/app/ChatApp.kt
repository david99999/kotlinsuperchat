package com.superchat.app

/**
 * Created by david on 23/02/16.
 */
import android.app.Application
import android.content.Intent
import com.mymvc.presenter.interfaces.communications.TheServer
import com.superchat.features.register.RegisterActivity
import com.superchat.utils.MyPreferenceManager


class ChatApp : Application() {

    private var pref: MyPreferenceManager? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        TheServer.init(this)
    }


    val prefManager: MyPreferenceManager
        get() {
            if (pref == null) {
                pref = MyPreferenceManager(this)
            }

            return pref as MyPreferenceManager
        }

    companion object {

        val TAG = ChatApp::class.java.simpleName

        var instance: ChatApp? = null
            private set
    }

    fun logout() {
        pref?.clear();
        var intent = Intent(this, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent);
    }
}