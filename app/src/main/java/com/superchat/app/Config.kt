package com.superchat.app

/**
 * Created by david on 23/02/16.
 */
// flag to identify whether to show single line
// or multi line text in push notification tray
const val appendNotificationMessages = true

// global topic to receive app wide push notifications
const val TOPIC_GLOBAL = "global"

// broadcast receiver intent filters
const val SENT_TOKEN_TO_SERVER = "sentTokenToServer"
const val REGISTRATION_COMPLETE = "registrationComplete"
const val PUSH_NOTIFICATION = "pushNotification"

// type of push messages
const val PUSH_TYPE_CHATROOM = 1
const val PUSH_TYPE_USER = 2

// id to handle the notification in the notification try
const val NOTIFICATION_ID = 100
const val NOTIFICATION_ID_BIG_IMAGE = 101
