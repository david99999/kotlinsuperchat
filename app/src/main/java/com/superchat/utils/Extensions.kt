package com.superchat.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by david on 2/03/16.
 */
inline fun Long.ToDateTime(): String {
    var text = ""
    try {
        var messageDate = Date(this * 1000)
        var today = Date()
        var sdf = SimpleDateFormat("yyyyMMdd")
        if (sdf.format(today).equals(sdf.format(messageDate))) {
            text = SimpleDateFormat("hh:mm a").format(messageDate)
        } else {
            text = SimpleDateFormat("dd LLL, hh:mm a").format(messageDate)
        }
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return text
}