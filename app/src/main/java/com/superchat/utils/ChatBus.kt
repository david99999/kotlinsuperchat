package com.superchat.utils

import com.squareup.otto.Bus

/**
 * Created by david on 15/03/16.
 */
interface ChatBus {
    companion object {
        var instance: Bus

        init {
            instance = Bus()
        }
    }
}