package com.superchat.utils.gcm

/**
 * Created by david on 23/02/16.
 */

import android.app.IntentService
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast

import com.google.android.gms.gcm.GcmPubSub
import com.google.android.gms.gcm.GoogleCloudMessaging
import com.google.android.gms.iid.InstanceID
import com.mymvc.presenter.interfaces.communications.TheServer
import com.superchat.R
import com.superchat.app.ChatApp
import com.superchat.app.SENT_TOKEN_TO_SERVER
import com.superchat.network.models.PushToken
import com.superchat.network.models.User
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

import java.io.IOException
import java.util.*


class GcmIntentService : IntentService(GcmIntentService.TAG) {


    override fun onHandleIntent(intent: Intent) {
        val key = intent.getStringExtra(KEY)
        when (key) {
            SUBSCRIBE -> {
                // subscribe to a topic
                val topic = intent.getStringExtra(TOPIC)
                subscribeToTopic(topic)
            }
            UNSUBSCRIBE -> {
                val topic1 = intent.getStringExtra(TOPIC)
                unsubscribeFromTopic(topic1)
            }
            else -> // if key is not specified, register with GCM
                registerGCM()
        }

    }

    /**
     * Registering with GCM and obtaining the gcm registration id
     */
    private fun registerGCM() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        var token: String? = null

        try {
            val instanceID = InstanceID.getInstance(this)
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null)

            Log.e(TAG, "GCM Registration Token: " + token!!)

            // sending the registration id to our server
            sendRegistrationToServer(token)

            sharedPreferences.edit().putBoolean(com.superchat.app.SENT_TOKEN_TO_SERVER, true).apply()
        } catch (e: Exception) {
            Log.e(TAG, "Failed to complete token refresh", e)

            sharedPreferences.edit().putBoolean(com.superchat.app.SENT_TOKEN_TO_SERVER, false).apply()
        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        val registrationComplete = Intent(com.superchat.app.REGISTRATION_COMPLETE)
        registrationComplete.putExtra("token", token)
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)
    }

    private fun sendRegistrationToServer(token: String) {
        // Send the registration token to our server
        // to keep it in MySQL
        var subscription = TheServer.instance.sendRegistrationToServer(
                ChatApp.instance!!.prefManager.getUser()?.userId.toString(),
                PushToken(token))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { answer ->
                    var registrationComplete = Intent(SENT_TOKEN_TO_SERVER)
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(registrationComplete)
                }
                .onErrorReturn { it: Throwable? ->
                    Toast.makeText(applicationContext, "error registrando en push", Toast.LENGTH_SHORT).show()
                    User()
                }
                .subscribe()
    }

    /**
     * Subscribe to a topic
     */
    fun subscribeToTopic(topic: String) {
        val pubSub = GcmPubSub.getInstance(applicationContext)
        val instanceID = InstanceID.getInstance(applicationContext)
        var token: String? = null
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null)
            if (token != null) {
                pubSub.subscribe(token, "/topics/" + topic, null)
                Log.e(TAG, "Subscribed to topic: " + topic)
            } else {
                Log.e(TAG, "error: gcm registration id is null")
            }
        } catch (e: IOException) {
            Log.e(TAG, "Topic subscribe error. Topic: " + topic + ", error: " + e.message)
            Toast.makeText(applicationContext, "Topic subscribe error. Topic: " + topic + ", error: " + e.message, Toast.LENGTH_SHORT).show()
        }

    }

    fun unsubscribeFromTopic(topic: String) {
        val pubSub = GcmPubSub.getInstance(applicationContext)
        val instanceID = InstanceID.getInstance(applicationContext)
        var token: String? = null
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null)
            if (token != null) {
                pubSub.unsubscribe(token, "")
                Log.e(TAG, "Unsubscribed from topic: " + topic)
            } else {
                Log.e(TAG, "error: gcm registration id is null")
            }
        } catch (e: IOException) {
            Log.e(TAG, "Topic unsubscribe error. Topic: " + topic + ", error: " + e.message)
            Toast.makeText(applicationContext, "Topic subscribe error. Topic: " + topic + ", error: " + e.message, Toast.LENGTH_SHORT).show()
        }

    }

    companion object {

        private val TAG = GcmIntentService::class.java.simpleName

        val KEY = "key"
        val TOPIC = "topic"
        val SUBSCRIBE = "subscribe"
        val UNSUBSCRIBE = "unsubscribe"
    }
}