package com.superchat.utils.gcm

/**
 * Created by david on 23/02/16.
 */

import android.content.Intent
import android.util.Log

import com.google.android.gms.iid.InstanceIDListenerService

class ChatIDListenerService : InstanceIDListenerService() {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    override fun onTokenRefresh() {
        Log.e(TAG, "onTokenRefresh")
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        val intent = Intent(this, GcmIntentService::class.java)
        startService(intent)
    }

    companion object {

        private val TAG = ChatIDListenerService::class.java.simpleName
    }
}