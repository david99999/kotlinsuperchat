package com.superchat.utils.gcm

/**
 * Created by david on 23/02/16.
 */

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.android.gms.gcm.GcmListenerService
import com.google.gson.Gson
import com.superchat.app.ChatApp
import com.superchat.app.PUSH_NOTIFICATION
import com.superchat.app.PUSH_TYPE_CHATROOM
import com.superchat.app.PUSH_TYPE_USER
import com.superchat.features.chatScreen.ChatScreenActivity
import com.superchat.network.models.Message
import com.superchat.network.models.User
import com.superchat.utils.NotificationUtils
import org.json.JSONException
import org.json.JSONObject


class ChatGcmPushReceiver : GcmListenerService() {

    private var notificationUtils: NotificationUtils? = null

    /**
     * Called when message is received.

     * @param from   SenderID of the sender.
     * *
     * @param bundle Data bundle containing message data as key/value pairs.
     * *               For Set of keys use data.keySet().
     */

    override fun onMessageReceived(from: String?, bundle: Bundle?) {
        var title = bundle?.getString("title");
        var isBackground = bundle?.getBoolean("is_background");
        var flag = bundle?.getString("flag");
        var data = bundle?.getString("data");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "title: " + title);
        Log.d(TAG, "isBackground: " + isBackground);
        Log.d(TAG, "flag: " + flag);
        Log.d(TAG, "data: " + data);

        if (flag == null)
            return;

        if (ChatApp.Companion.instance?.prefManager?.getUser() == null) {
            // user is not logged in, skipping push notification
            Log.e(TAG, "user is not logged in, skipping push notification");
            return;
        }

        if (from?.startsWith("/topics/")!!) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        when (Integer.parseInt(flag)) {
            PUSH_TYPE_CHATROOM ->
                // push notification belongs to a chat room
                processChatRoomPush(title!!, isBackground!!, Gson().fromJson(data!!, Message::class.java));

            PUSH_TYPE_USER ->
                // push notification is specific to user
                processUserMessage(title!!, isBackground!!, data!!);
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context, title: String, message: String, timeStamp: String, intent: Intent) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(context: Context, title: String, message: String, timeStamp: String, intent: Intent, imageUrl: String) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    companion object {

        private val TAG = ChatGcmPushReceiver::class.java.simpleName
    }

    /**
     * Processing chat room push message
     * this message will be broadcasts to all the activities registered
     */
    private fun processChatRoomPush(title: String, isBackground: Boolean, data: Message) {
        if (!isBackground) {

            try {

                // skip the message if the message belongs to same user as
                // the user would be having the same message when he was sending
                // but it might differs in your scenario
                if (data.user.userId.equals(ChatApp.Companion.instance?.prefManager?.getUser()?.userId)) {
                    Log.e(TAG, "Skipping the push message as it belongs to same user")
                    return
                }


                // verifying whether the app is in background or foreground
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                    // app is in foreground, broadcast the push message
                    val pushNotification = Intent(PUSH_NOTIFICATION)
                    pushNotification.putExtra("type", PUSH_TYPE_CHATROOM)
                    pushNotification.putExtra("message", data)
                    pushNotification.putExtra("chat_room_id", data.chatRoomId.toString())
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

                    // play notification sound
                    val notificationUtils = NotificationUtils()
                    notificationUtils.playNotificationSound()
                } else {

                    // app is in background. show the message in notification try
                    val resultIntent = Intent(applicationContext, ChatScreenActivity::class.java)
                    resultIntent.putExtra("chat_room_id", data.chatRoomId.toString())
                    showNotificationMessage(applicationContext, title,
                            data.user.name + " : " + data.message, data.createdAt.toString(), resultIntent)
                }

            } catch (e: JSONException) {
                Log.e(TAG, "json parsing error: " + e.message)
                Toast.makeText(applicationContext, "Json parse error: " + e.message, Toast.LENGTH_LONG).show()
            }

        } else {
            // the push notification is silent, may be other operations needed
            // like inserting it in to SQLite
        }
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */
    private fun processUserMessage(title: String, isBackground: Boolean, data: String) {
        if (!isBackground) {

            try {
                val datObj = JSONObject(data)

                val imageUrl = datObj.getString("image")

                val mObj = datObj.getJSONObject("message")
                val message = Message(
                        message = mObj.getString("message"),
                        messageId = mObj.getString("message_id").toInt(),
                        createdAt = mObj.getString("created_at").toLong(),
                        user = User())

                val uObj = datObj.getJSONObject("user")
                val user = User(
                        userId = uObj.getInt("user_id"),
                        email = uObj.getString("email"),
                        name = uObj.getString("name"))
                message.user = user

                // verifying whether the app is in background or foreground
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                    // app is in foreground, broadcast the push message
                    val pushNotification = Intent(PUSH_NOTIFICATION)
                    pushNotification.putExtra("type", PUSH_TYPE_USER)
                    pushNotification.putExtra("message", message)
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

                    // play notification sound
                    val notificationUtils = NotificationUtils()
                    notificationUtils.playNotificationSound()
                } else {

                    // app is in background. show the message in notification try
                    val resultIntent = Intent(applicationContext, ChatScreenActivity::class.java)

                    // check for push notification image attachment
                    if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(applicationContext, title,
                                user.name + " : " + message.message, message.createdAt.toString(), resultIntent)
                    } else {
                        // push notification contains image
                        // show it with the image
                        showNotificationMessageWithBigImage(applicationContext,
                                title, message.message, message.createdAt.toString(), resultIntent, imageUrl)
                    }
                }
            } catch (e: JSONException) {
                Log.e(TAG, "json parsing error: " + e.message)
                Toast.makeText(getApplicationContext(), "Json parse error: " + e.message, Toast.LENGTH_LONG).show()
            }

        } else {
            // the push notification is silent, may be other operations needed
            // like inserting it in to SQLite
        }
    }

}