package com.superchat.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mymvc.presenter.interfaces.communications.TheServer;
import com.superchat.network.models.ChatRoom;
import com.superchat.network.models.User;

import java.util.ArrayList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by david on 15/03/16.
 */
public class ListAllChatGroupsService extends Service {
    private Subscription mySubscription;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        User user = (User) intent.getExtras().getSerializable("user");
        Log.i("tadaa", "recibido el usuario " + user.getName());
        //Toast.makeText(this, "Ha inciado el servicio", Toast.LENGTH_SHORT).show();
        Log.i("tadaa", "iniciado");
        mySubscription = TheServer.Companion.getInstance().FetchChats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<ArrayList<ChatRoom>>() {
                    @Override
                    public void call(ArrayList<ChatRoom> chatRooms) {
                        Log.i("tadaa", "traidos " + chatRooms.size());
                        ChatBus.Companion.getInstance().post(chatRooms);
                        stopSelf();
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                })
                .subscribe();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("tadaa", "destruido el servicio");
        mySubscription.unsubscribe();
        super.onDestroy();

    }
}
