package com.superchat.utils

/**
 * Created by david on 23/02/16.
 */
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.superchat.network.models.User

class MyPreferenceManager(internal var _context: Context) {

    private val TAG = MyPreferenceManager::class.java.simpleName

    // Shared Preferences
    internal var pref: SharedPreferences

    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor

    // Shared pref mode
    internal var PRIVATE_MODE = 0

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun addNotification(notification: String) {

        // get old notifications
        var oldNotifications: String? = notifications

        if (oldNotifications != null) {
            oldNotifications += "|" + notification
        } else {
            oldNotifications = notification
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications)
        editor.commit()
    }

    val notifications: String
        get() = pref.getString(KEY_NOTIFICATIONS, "")

    fun clear() {
        editor.clear()
        editor.commit()
    }

    companion object {

        // Sharedpref file name
        private val PREF_NAME = "androidhive_gcm"

        // All Shared Preferences Keys
        private val KEY_USER_ID = "user_id"
        private val KEY_USER_NAME = "user_name"
        private val KEY_USER_EMAIL = "user_email"
        private val KEY_NOTIFICATIONS = "notifications"
    }


    fun storeUser(user: User) {
        editor.putString(KEY_USER_ID, user.userId.toString());
        editor.putString(KEY_USER_NAME, user.name);
        editor.putString(KEY_USER_EMAIL, user.email);
        editor.commit();

        Log.e(TAG, "User is stored in shared preferences. " + user.name + ", " + user.email);
    }

    fun getUser(): User? {
        if (pref.getString(KEY_USER_ID, null) != null) {
            var userId = pref.getString(KEY_USER_ID, null).toInt();
            var name = pref.getString(KEY_USER_NAME, null);
            var email = pref.getString(KEY_USER_EMAIL, null);
            return User(name, email, userId);
        }
        return null
    }
}