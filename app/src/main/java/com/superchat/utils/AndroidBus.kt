package com.superchat.utils

import android.os.Handler
import android.os.Looper

import com.squareup.otto.Bus

/**
 * Created by david on 15/03/16.
 */
class AndroidBus : Bus() {
    private val mHandler = Handler(Looper.getMainLooper())

    override fun post(event: Any) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event)
        } else {
            mHandler.post { super@AndroidBus.post(event) }
        }
    }
}
